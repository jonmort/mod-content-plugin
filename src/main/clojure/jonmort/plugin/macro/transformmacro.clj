(ns jonmort.plugin.macro.transformmacro
  (:require [net.cgrand.enlive-html :as html])
  (:import (com.atlassian.confluence.macro Macro))
  (:gen-class :implements [com.atlassian.confluence.macro.Macro
                           com.atlassian.confluence.content.render.xhtml.transformers.Transformer]
              :main false))

(defn- render-snippet [snippet]
  "Render the snippet as a string."
  (apply str (html/emit* snippet)))

(defn- lower-heading [node]
  "Take a heading node and reduce it by one level"
  (let [heading-reduce-map {:h1 :h2 :h2 :h3 :h3 :h4 :h4 :h5 :h5 :h6 :h6 :p}
        new-tag ((:tag node) heading-reduce-map)]
    (assoc node :tag new-tag)))

(def all-headings
  "Equivalent of :headings"
  #{:h1 :h2 :h3 :h4 :h5 :h6})

(def reduce-heading
  "Lowers the heading level by one"
  [[[all-headings] lower-heading]])

(defn- unwrap-elem [^:keyword elem]
  "Returns the transformation to unwrap elem"
  [[[elem] html/unwrap]])

(def unwrap-div (unwrap-elem :div))

(def unwrap-p (unwrap-elem :p))

(def ^:dynamic fix
  "Ensures that headings onl contain text and removes spans with a style"
  [[[all-headings :> html/any-node] html/text]
   [[[:span (html/attr? :style)]] html/unwrap]])

(defn ^:dynamic process-body [^String body t]
  "Take the body as a string and apply the transformation t using html/at*.
   t must be a list of the transformations"
    (-> body
         html/html-snippet
         (html/at* t)
         render-snippet))

(defn ^:dynamic execute [params ^String body _]
  "Implementation of the execute function of Macro.
   Takes the macro body and transforms it using the transform-function.
   The default action is to call reduce-heading"
  (let [t (-> (or (get params "transform-function") "reduce-heading")
              str
              symbol
              resolve
              var-get)]
    (process-body body t)))

(defn ^:dynamic transform [input context]
  "Implementation of the transformer transform function.
   Applys the fix to the entire document. Can be used on render or save."
  (with-open [rdr (clojure.java.io/reader input)]
    (let [s (clojure.string/join "\n" (line-seq rdr))]
      (process-body s fix))))

(defn -execute [_ params body context]
  ;; need to have this binding otherwise *ns* is 'clojure.core which is not very helpful
  (binding [*ns* 'jonmort.plugin.macro.transformmacro]
      (execute params body context)))

(defn -getBodyType [_]
  com.atlassian.confluence.macro.Macro$BodyType/RICH_TEXT)

(defn -getOutputType [_]
  com.atlassian.confluence.macro.Macro$OutputType/BLOCK)

(defn -transform [_ input context]
  (binding [*ns* 'jonmort.plugin.macro.transformmacro]
    (transform input context)))
